#!/bin/bash

COMPRESS_CMD=7z

if ! command -v ${COMPRESS_CMD} &> /dev/null; then
    echo -e "${COMPRESS_CMD} not found"
    exit 1
fi

usage()
{
    echo -e "Usage: $0
        -s, -source, --source
            Source file or directory to compress

        -d, -dest, --dest
            Destination path compressed file

        -n, -name, --name
            Set file name for target compressed 

        -h, -help, --help
            Show this help"
    exit 1
}

if [[ $# -eq 0 ]]; then
    usage
fi

while [[ $# -gt 0 ]]; do
    key="$1"

    case $key in
        -s|-source|--source)
            SOURCE_PATH=$2
            shift 2
        ;;
        -d|-dest|--dest)
            DEST_PATH=$2
            shift 2
        ;;
        -n|-name|--name)
            DEST_NAME=$2
            shift 2
        ;;
        -h|-help|--help)
            usage
            shift
        ;;
        *)
            shift
        ;;
    esac
done

if [ -z ${SOURCE_PATH} ] || [ -z ${DEST_PATH} ]; then
    echo -e "Error: source or destination path not defined\n"
    usage
fi

if [ ! -d ${DEST_PATH} ]; then
    mkdir -p ${DEST_PATH}
fi

if [ -z ${DEST_NAME} ]; then
    FILE_PATH=$(realpath ${DEST_PATH}/$(basename ${SOURCE_PATH}))
else
    FILE_PATH=$(realpath ${DEST_PATH}/${DEST_NAME}).7z
fi

${COMPRESS_CMD} a -t7z -m0=lzma -mx=9 -mfb=258 -md=32m -ms=on ${FILE_PATH} ${SOURCE_PATH}
