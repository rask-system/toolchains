#!/bin/bash

usage()
{
    echo -e "Usage: $0
        -t, -toolchain, --toolchain
            GCC toolchain <arm-s920-linux-gnueabi>

        -m, -mkspec (only Qt5), --mkspec
            mkspec (only Qt5)

        -q, -qt, --qt
            Qt version <5.15.2, 6.2.2>

        -d, -device, --device
            Device (s920) (only Qt6)

        -p, -patch, --patch
            Applay patch

        -h, -help, --help
            Show this help"
    exit 1
}

if [[ $# -eq 0 ]]; then
    usage
fi

while [[ $# -gt 0 ]]; do
    key="$1"

    case $key in
        -q|-qt|--qt)
            export QT_VERSION=$2
            shift 2
        ;;
        -m|-mkspec|--mkspec) 
            export QT_MKSPECS=$2
            shift 2
        ;;
        -t|-toolchain|--toolchain) 
            export TOOLCHAIN=$2
            shift 2
        ;;
        -d|-device|--device)
            export DEVICE=$2
            shift 2
        ;;
        -p|-patch|--patch)
            export APPLY_PATCH=true
            shift
        ;;
        -h|-help|--help) 
            usage 
        ;;
        *)
            shift
        ;;
    esac
done
shift $((OPTIND-1))

if [ -z "${QT_VERSION}" ]; then
    echo -e "Qt version not defined\t"
    usage
fi

export QT_SRC=src
QT_BASE_VERSION=$(echo ${QT_VERSION} | awk -F'.' '{print $1"."$2}')
QT_SRC_BASENAME="qt-everywhere-src-${QT_VERSION}"
QT_SRC_FILE="${QT_SRC_BASENAME}.tar.xz"

echo ${QT_SRC_FILE}
if [ ! -f ${QT_SRC_FILE} ]; then
    wget https://download.qt.io/archive/qt/${QT_BASE_VERSION}/${QT_VERSION}/single/${QT_SRC_FILE} || exit 1
fi

rm -rfv ${QT_SRC} ${QT_SRC_BASENAME}

tar -xvf ${QT_SRC_FILE} && \
    mv -v ${QT_SRC_BASENAME} ${QT_SRC} || exit 1

if [ "${APPLY_PATCH}" = true ]; then
    cd ./patch
    ./apply-patch.sh $QT_VERSION $TOOLCHAIN || exit 1
    cd ..
fi

cd $QT_SRC

TOOLCHAIN_PATH=/opt/toolchains/$TOOLCHAIN
SYSROOT=$TOOLCHAIN_PATH/$TOOLCHAIN/sysroot

if [[ $QT_VERSION == 5* ]]; then
    if [ -z "$QT_MKSPECS" ] || [ -z "$TOOLCHAIN" ]; then
        echo -e "mkspecs or toolchain not defined\n"
        usage
    fi
    source ../build-qt5.sh
elif [[ $QT_VERSION == 6* ]]; then
    if [ -z "$TOOLCHAIN" ] || [ -z "$DEVICE" ]; then
        echo -e "toolchain file or device not defined\n"
        usage
    fi
    source ../build-qt6.sh
fi
