#!/bin/bash

QT_VERSION=$1
shift

if [ -z $QT_VERSION ]; then
    echo "Qt version patch not defined"
    exit 1
fi

if [ ! -d $QT_VERSION ]; then
    echo "Qt version patch not found"
    exit 1
fi

cd $QT_VERSION

DEVICES=("common")
if [ ! -z $1 ]; then
    DEVICES+=($1)
    shift
fi

echo "Qt Version ${QT_VERSION}"

for DEVICE in ${DEVICES[@]}; do
    if [ ! -d $DEVICE ]; then continue; fi
    cd $DEVICE
    echo -e "\nPatch for device: ${DEVICE}"

    for PATCH in $(find . -name "*.patch" -type f); do
        fileTarget="${PATCH%.*}"
        dir=$(dirname ../../../${QT_SRC}/${PATCH})
        echo "Target $fileTarget"

        patch -ruN -d $dir < ${PATCH}
    done

    cd ..
done

