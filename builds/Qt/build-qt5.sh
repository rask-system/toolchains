#!/bin/bash

echo "Qt: $QT_VERSION mkspecs: $QT_MKSPECS toolchain: $TOOLCHAIN"

COMPILER_PATH=$TOOLCHAIN_PATH/bin
CROSS_COMPILE=$COMPILER_PATH/$TOOLCHAIN-
SKIP_MODULES=
NO_FEATURES_MODULES=

case $QT_VERSION in
5.12.10) 
    SKIP_MODULES="\
        -skip qt3d \
        -skip qtcanvas3d \
        -skip qtcharts \
        -skip qtconnectivity \
        -skip qtdoc \
        -skip qtgamepad \
        -skip qtlocation \
        -skip qtmultimedia \
        -skip qtnetworkauth \
        -skip qtpurchasing \
        -skip qtsensors \
        -skip qtserialbus \
        -skip qtserialport \
        -skip qtspeech \
        -skip qtscript \
        -skip qttools \
        -skip qtscxml \
        -skip qtxmlpatterns \
        -skip qttranslations \
        -skip qtwayland \
        -skip qtwebengine \
        -skip qtwebview \
        -skip qtwebchannel \
        -skip qtwebglplugin \
        -skip qtwebsockets
    "
    NO_FEATURES_MODULES="\
        -no-feature-abstractbutton \
        -no-feature-accessibility \
        -no-feature-accessibility-atspi-bridge \
        -no-feature-action \
        -no-feature-android-style-assets \
        -no-feature-angle \
        -no-feature-angle_d3d11_qdtd \
        -no-feature-appstore-compliant \
        -no-feature-avx2 \
        -no-feature-bearermanagement \
        -no-feature-big_codecs \
        -no-feature-dbus \
        -no-feature-dbus-linked \
        -no-feature-cssparser \
        -no-feature-cupsjobwidget \
        -no-feature-direct2d \
        -no-feature-direct2d1_1 \
        -no-feature-direct3d11 \
        -no-feature-direct3d11_1 \
        -no-feature-direct3d9 \
        -no-feature-directfb \
        -no-feature-directwrite \
        -no-feature-directwrite1 \
        -no-feature-directwrite2 \
        -no-feature-drm_atomic \
        -no-feature-debug_and_release \
        -no-feature-desktopservices \
        -no-feature-dxgi \
        -no-feature-dxgi1_2 \
        -no-feature-dxguid \
        -no-feature-effects \
        -no-feature-egl \
        -no-feature-egl_x11 \
        -no-feature-eglfs \
        -no-feature-eglfs_brcm \
        -no-feature-eglfs_egldevice \
        -no-feature-eglfs_gbm \
        -no-feature-eglfs_mali \
        -no-feature-eglfs_openwfd \
        -no-feature-eglfs_rcar \
        -no-feature-eglfs_viv \
        -no-feature-eglfs_viv_wl \
        -no-feature-eglfs_vsp2 \
        -no-feature-eglfs_x11 \
        -no-feature-glibc \
        -no-feature-gnu-libiconv \
        -no-feature-harfbuzz \
        -no-feature-gtk3 \
        -no-feature-opengles2 \
        -no-feature-opengles3 \
        -no-feature-opengles31 \
        -no-feature-opengles32 \
        -no-feature-paint_debug \
        -no-feature-pdf \
        -no-feature-pkg-config \
        -no-feature-qml-debug \
        -no-feature-qqnx_imf \
        -no-feature-qqnx_pps \
        -no-feature-quickcontrols2-fusion \
        -no-feature-quickcontrols2-imagine \
        -no-feature-quickcontrols2-material \
        -no-feature-quickcontrols2-universal \
        -no-feature-system-doubleconversion \
        -no-feature-system-freetype \
        -no-feature-system-harfbuzz \
        -no-feature-system-jpeg \
        -no-feature-system-pcre2 \
        -no-feature-system-png \
        -no-feature-system-proxies \
        -no-feature-system-xcb \
        -no-feature-system-zlib \
        -no-feature-systemtrayicon \
        -no-feature-testlib \
        -no-feature-whatsthis \
        -no-feature-wheelevent \
        -no-feature-wizard \
        -no-feature-xcb \
        -no-feature-xcb-egl-plugin \
        -no-feature-xcb-glx \
        -no-feature-xcb-glx-plugin \
        -no-feature-xcb-native-painting \
        -no-feature-xcb-sm \
        -no-feature-xcb-xinput \
        -no-feature-xcb-xlib \
        -no-feature-xkb \
        -no-feature-xkbcommon \
        -no-feature-xlib \
        -no-pch
    "
    ;;
5.15.2)
    SKIP_MODULES="\
        -skip qt3d \
        -skip qtcanvas3d \
        -skip qtcharts \
        -skip qtconnectivity \
        -skip qtdoc \
        -skip qtdocgallery \
        -skip qtgamepad \
        -skip qtlocation \
        -skip qtlottie \
        -skip qtmultimedia \
        -skip qtnetworkauth \
        -skip qtquick3d \
        -skip qtpurchasing \
        -skip qtscript \
        -skip qtsensors \
        -skip qtscxml \
        -skip qtserialbus \
        -skip qtserialport \
        -skip qtspeech \
        -skip qttranslations \
        -skip qttools \
        -skip qtxmlpatterns \
        -skip qttranslations \
        -skip qtwayland \
        -skip qtwebengine \
        -skip qtwebview \
        -skip qtwebchannel \
        -skip qtwebglplugin \
        -skip qtwebsockets
    "
    NO_FEATURES_MODULES="\
        -no-feature-abstractbutton \
        -no-feature-accessibility \
        -no-feature-accessibility-atspi-bridge \
        -no-feature-action \
        -no-feature-android-style-assets \
        -no-feature-angle \
        -no-feature-angle_d3d11_qdtd \
        -no-feature-appstore-compliant \
        -no-feature-avx2 \
        -no-feature-bearermanagement \
        -no-feature-big_codecs \
        -no-feature-dbus \
        -no-feature-dbus-linked \
        -no-feature-cssparser \
        -no-feature-cupsjobwidget \
        -no-feature-direct2d \
        -no-feature-direct2d1_1 \
        -no-feature-direct3d11 \
        -no-feature-direct3d11_1 \
        -no-feature-direct3d9 \
        -no-feature-directfb \
        -no-feature-directwrite \
        -no-feature-directwrite1 \
        -no-feature-directwrite2 \
        -no-feature-drm_atomic \
        -no-feature-debug_and_release \
        -no-feature-desktopservices \
        -no-feature-dxgi \
        -no-feature-dxgi1_2 \
        -no-feature-dxguid \
        -no-feature-effects \
        -no-feature-egl \
        -no-feature-egl_x11 \
        -no-feature-eglfs \
        -no-feature-eglfs_brcm \
        -no-feature-eglfs_egldevice \
        -no-feature-eglfs_gbm \
        -no-feature-eglfs_mali \
        -no-feature-eglfs_openwfd \
        -no-feature-eglfs_rcar \
        -no-feature-eglfs_viv \
        -no-feature-eglfs_viv_wl \
        -no-feature-eglfs_vsp2 \
        -no-feature-eglfs_x11 \
        -no-feature-glibc \
        -no-feature-gnu-libiconv \
        -no-feature-harfbuzz \
        -no-feature-gtk3 \
        -no-feature-opengles2 \
        -no-feature-opengles3 \
        -no-feature-opengles31 \
        -no-feature-opengles32 \
        -no-feature-pdf \
        -no-feature-pkg-config \
        -no-feature-qml-debug \
        -no-feature-qqnx_imf \
        -no-feature-qqnx_pps \
        -no-feature-quick-designer \
        -no-feature-quick-particles \
        -no-feature-quick-path \
        -no-feature-quick-pathview \
        -no-feature-quick-sprite \
        -no-feature-quick-shadereffect \
        -no-feature-quickcontrols2-fusion \
        -no-feature-quickcontrols2-imagine \
        -no-feature-quickcontrols2-material \
        -no-feature-quickcontrols2-universal \
        -no-feature-texthtmlparser \
        -no-feature-textmarkdownreader \
        -no-feature-textmarkdownwriter \
        -no-feature-textodfwriter \
        -no-feature-system-doubleconversion \
        -no-feature-system-freetype \
        -no-feature-system-harfbuzz \
        -no-feature-system-jpeg \
        -no-feature-system-pcre2 \
        -no-feature-system-png \
        -no-feature-system-proxies \
        -no-feature-system-zlib \
        -no-feature-systemtrayicon \
        -no-feature-testlib \
        -no-feature-tuiotouch \
        -no-feature-whatsthis \
        -no-feature-wheelevent \
        -no-feature-wizard \
        -no-feature-xcb \
        -no-feature-xcb-egl-plugin \
        -no-feature-xcb-glx \
        -no-feature-xcb-glx-plugin \
        -no-feature-xcb-native-painting \
        -no-feature-xcb-sm \
        -no-feature-xcb-xlib \
        -no-feature-xkbcommon \
        -no-feature-xlib
    "
    ;;
*) echo "Available versions: 5.12.10 5.15.2" ;;
esac

cp -r ../Qt5/mkspecs ./qtbase/

# export WOLFSSL_LIBS="-L${SYSROOT}/usr/lib -lwolfssl"
export OPENSSL_LIBS="-L${SYSROOT}/usr/lib -lssl -lcrypto"

./configure \
    -device $QT_MKSPECS \
    -linuxfb \
    -device-option CROSS_COMPILE=$CROSS_COMPILE \
    -sysroot $SYSROOT \
    -opensource -confirm-license \
    -reduce-exports -release -optimize-size \
    -make libs \
    -ltcg \
    -no-cups \
    -no-opengl \
    -no-feature-xml \
    -no-feature-widgets \
    -no-pch \
    -no-gcc-sysroot \
    -no-use-gold-linker -nomake examples -nomake tests -nomake tools \
    $SKIP_MODULES $NO_FEATURES_MODULES \
    -openssl-linked -I${SYSROOT}/usr/include \
    -prefix /usr/local/qt5-openssl \
    --recheck-all || exit 1

# -wolfssl-linked -I${SYSROOT}/usr/include/wolfssl \
# -prefix /usr/local/qt5 \
# -reduce-exports -release -optimize-size \

startDate=$(date +%s)
make -j$(nproc) && make install
endDate=$(date +%s)

echo $((endDate - startDate)) | awk '{print int($1/60)":"int($1%60)}'