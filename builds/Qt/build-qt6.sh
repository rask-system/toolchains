#!/bin/bash

cp -r ../Qt6/mkspecs ./qtbase/

echo "Qt: $QT_VERSION toolchain: $TOOLCHAIN"

BUILD_DIR=build
if [ -d $BUILD_DIR ]; then
    rm -rfv $BUILD_DIR
fi
mkdir -p $BUILD_DIR

CMAKE_TOOLCHAIN=$TOOLCHAIN_PATH/libexec/cmake/toolchain.cmake

cmake -B ${BUILD_DIR} -GNinja -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_TOOLCHAIN_FILE=${CMAKE_TOOLCHAIN} \
    -DCMAKE_INSTALL_PREFIX=/opt/Qt/${QT_VERSION}/${DEVICE} \
    -DCMAKE_STAGING_PREFIX=/opt/Qt/${QT_VERSION}/${DEVICE} \
    -DCMAKE_INTERPROCEDURAL_OPTIMIZATION_RELEASE=ON \
    \
    -DQT_HOST_PATH=/opt/Qt/${QT_VERSION}/gcc_64 \
    -DQT_QMAKE_TARGET_MKSPEC=devices/linux-arm-generic-g++ \
    -DQT_BUILD_EXAMPLES=FALSE \
    -DQT_BUILD_TESTS=FALSE \
    -DQT_BUILD_TESTS_BY_DEFAULT=OFF \
    -DQT_QMAKE_DEVICE_OPTIONS=CROSS_COMPILE=${TOOLCHAIN}- \
    \
    -DBUILD_WITH_PCH=OFF \
    -DBUILD_qt3d=OFF \
    -DBUILD_qtcharts=OFF \
    -DBUILD_qtconnectivity=ON \
    -DBUILD_qtdoc=OFF \
    -DBUILD_qtlottie=OFF \
    -DBUILD_qtmultimedia=OFF \
    -DBUILD_qtquick3d=OFF \
    -DBUILD_qtsensors=OFF \
    -DBUILD_qtscxml=OFF \
    -DBUILD_qtserialbus=OFF \
    -DBUILD_qtserialport=OFF \
    -DBUILD_qtwayland=OFF \
    -DBUILD_qtwebengine=OFF \
    -DBUILD_qtwebview=OFF \
    -DBUILD_qtwebchannel=OFF \
    -DBUILD_qtwebsockets=OFF \
    -DBUILD_qtopcua=OFF \
    -DBUILD_qtmqtt=ON \
    -DBUILD_qtcoap=ON \
    -DBUILD_qtpositioning=OFF \
    -DBUILD_qtremoteobjects=ON \
    -DBUILD_qtnetworkauth=OFF \
    -DBUILD_qt5compat=OFF \
    -DBUILD_qtquicktimeline=OFF \
    \
    -DINPUT_reduce_exports=yes \
    -DINPUT_optimize_size=yes \
    -DINPUT_opengl=no \
    -DINPUT_use_gold_linker_alias=no \
    -DINPUT_dbus=no \
    -DINPUT_egl=no \
    -DINPUT_egl_x11=no \
    -DINPUT_eglfs=no \
    -DINPUT_eglfs_brcm=no \
    -DINPUT_eglfs_egldevice=no \
    -DINPUT_eglfs_gbm=no \
    -DINPUT_eglfs_mali=no \
    -DINPUT_eglfs_openwfd=no \
    -DINPUT_eglfs_rcar=no \
    -DINPUT_eglfs_viv=no \
    -DINPUT_eglfs_viv_wl=no \
    -DINPUT_eglfs_vsp2=no \
    -DINPUT_eglfs_x11=no \
    -DINPUT_opengles2=no \
    -DINPUT_opengles3=no \
    -DINPUT_opengles31=no \
    -DINPUT_opengles32=no \
    -DINPUT_quickcontrols2_fusion=no \
    -DINPUT_quickcontrols2_imagine=no \
    -DINPUT_quickcontrols2_universal=no \
    -DINPUT_quickcontrols2_macos=no \
    -DINPUT_quickcontrols2_windows=no \
    \
    -DINPUT_textmarkdownreader=no \
    -DINPUT_textmarkdownwriter=no \
    -DINPUT_textodfwriter=no \
    -DINPUT_direct2d=no \
    -DINPUT_direct2d1_1=no \
    -DINPUT_directfb=no \
    -DINPUT_directwrite=no \
    -DINPUT_drm_atomic=no \
    -DINPUT_desktopservices=no \
    -DINPUT_vnc=no \
    -DINPUT_xcb=no \
    -DINPUT_xcb_egl_plugin=no \
    -DINPUT_xcb_glx=no \
    -DINPUT_xcb_glx_plugin=no \
    -DINPUT_xcb_sm=no \
    -DINPUT_xcb_xlib=no \
    -DINPUT_xkbcommon=no \
    -DINPUT_xlib=no \
    -DINPUT_libudev=no \
    -DINPUT_pdf=no \
    || exit 1

cmake --build ${BUILD_DIR} --parallel || exit 1
cmake --install ${BUILD_DIR} || exit 1