#!/bin/bash

rm -rf ./src
tar -xf mtdev-1.1.6.tar.gz
mv mtdev-1.1.6 src
cd ./src

CROSS_UTILS=arm-s920-linux-gnueabi
SYSROOT=/opt/toolchains/${CROSS_UTILS}/${CROSS_UTILS}/sysroot

export PATH="$PATH:/opt/toolchains/${CROSS_UTILS}/bin"
export CC=${CROSS_UTILS}-gcc
export AR=${CROSS_UTILS}-ar
export RANLIB=${CROSS_UTILS}-ranlib
export LD=${CROSS_UTILS}-ld
export CFLAGS+="-fPIC -no-pie"

./configure \
    --build=${CROSS_UTILS} \
    --host=x86_64-build_pc-linux-gnu \
    --target=${CROSS_UTILS} \
    --with-sysroot=${SYSROOT} \
    --prefix=${SYSROOT}/usr \
    || exit 1

make && make install
