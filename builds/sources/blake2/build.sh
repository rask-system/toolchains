#!/bin/bash

cd ./src

# CROSS_UTILS=arm-verifone-linux-gnueabihf
CROSS_UTILS=arm-s920-linux-gnueabi
SYSROOT=/opt/toolchains/${CROSS_UTILS}/${CROSS_UTILS}/sysroot

BRANCH_BUILD="build"
git checkout master
git branch -d $BRANCH_BUILD
git clean -dfx || exit 1
git checkout . || exit 1
git checkout -b $BRANCH_BUILD

export PATH="$PATH:/opt/toolchains/${CROSS_UTILS}/bin"
export CC=${CROSS_UTILS}-gcc
export AR=${CROSS_UTILS}-ar
export RANLIB=${CROSS_UTILS}-ranlib
export LD=${CROSS_UTILS}-ld
export CFLAGS+="-fPIC -no-pie"

./autogen.sh || exit 1
./configure \
    --build=${CROSS_UTILS} \
    --host=x86_64-build_pc-linux-gnu \
    --target=${CROSS_UTILS} \
    --with-sysroot=${SYSROOT} \
    --prefix=${SYSROOT}/usr \
    || exit 1

make && make install
