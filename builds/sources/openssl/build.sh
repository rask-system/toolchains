#!/bin/bash

cd ./src

CROSS_UTILS=arm-s920-linux-gnueabi
SYSROOT=/opt/toolchains/${CROSS_UTILS}/${CROSS_UTILS}/sysroot

BRANCH_BUILD="build"
git checkout master
git branch -d $BRANCH_BUILD
git clean -dfx || exit 1
git checkout . || exit 1
git checkout -b $BRANCH_BUILD

export PATH="$PATH:/opt/toolchains/${CROSS_UTILS}/bin"
export CC=${CROSS_UTILS}-gcc
export AR=${CROSS_UTILS}-ar
export RANLIB=${CROSS_UTILS}-ranlib
export LD=${CROSS_UTILS}-ld

# ./autogen.sh || exit 1
# ./configure --help && exit 1

./Configure --prefix=${SYSROOT}/usr -march=armv6 -L${SYSROOT}/usr/lib -L${SYSROOT}/usr/lib linux-armv4 shared
make clean && make -j$(nproc) && make install
