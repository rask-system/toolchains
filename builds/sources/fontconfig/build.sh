#!/bin/bash

cd ./src

CROSS_UTILS=arm-s920-linux-gnueabi
SYSROOT=/opt/toolchains/${CROSS_UTILS}/${CROSS_UTILS}/sysroot

export PATH="$PATH:/opt/toolchains/${CROSS_UTILS}/bin"
export CC=${CROSS_UTILS}-gcc
export AR=${CROSS_UTILS}-ar
export RANLIB=${CROSS_UTILS}-ranlib
export LD=${CROSS_UTILS}-ld

./configure \
    --build=${CROSS_UTILS} \
    --host=arm-unknown-linux-gnu \
    --with-sysroot=${SYSROOT} \
    --with-arch=arm \
    --with-gnu-ld \
    --prefix=${SYSROOT}/usr \
    || exit 1

make && make install
