#!/bin/bash

cd ./src

# CROSS_UTILS=arm-verifone-linux-gnueabihf
CROSS_UTILS=arm-s920-linux-gnueabi
SYSROOT=/opt/toolchains/${CROSS_UTILS}/${CROSS_UTILS}/sysroot
TOOLCHAIN_PATH=/opt/toolchains/${CROSS_UTILS}/libexec/cmake/toolchain.cmake
BUILD_PATH=build

CMAKE_INSTALL_PREFIX=${SYSROOT}/usr
CMAKE_INSTALL_INCLUDEDIR=${CMAKE_INSTALL_PREFIX}/include

rm -rf ${BUILD_PATH}

cmake -B ${BUILD_PATH} \
    -DCMAKE_BUILD_TYPE=Release \
    -GNinja \
    -DCMAKE_TOOLCHAIN_FILE=${TOOLCHAIN_PATH} \
    -DCMAKE_INSTALL_PREFIX=${CMAKE_INSTALL_PREFIX} \
    || exit 1

cmake --build ${BUILD_PATH}
cmake --build build --target install