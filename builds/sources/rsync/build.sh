#!/bin/bash
BUILD_HOST=/home/marssola/Developer/x-tools/toolchains/arm-verifone-linux-gnueabihf/arm-verifone-linux-gnueabihf/sysroot
ARM_GCC=/home/marssola/Developer/x-tools/toolchains/arm-verifone-linux-gnueabihf/bin/arm-verifone-linux-gnueabihf-gcc
BASE=`pwd`
OUTPUT_PATH=${BASE}/install
RSYNC=rsync-3.2.3

export PATH="$PATH:/home/marssola/Developer/x-tools/toolchains/arm-verifone-linux-gnueabihf/bin"

make_dirs () {
    #In order to facilitate management, create related directories
    cd ${BASE} && mkdir compressed install source -p
}
download_package () {
    cd ${BASE}/compressed
    #Download package
    wget https://download.samba.org/pub/rsync/${RSYNC}.tar.gz || exit 1
}
tar_package () {
    cd ${BASE}/compressed
    ls * > /tmp/list.txt
    for TAR in `cat /tmp/list.txt`
    do
        tar -xf $TAR -C  ../source
    done
    rm -rf /tmp/list.txt
}
make_rsync () {
	cd ${BASE}/source/${RSYNC}
	./configure --prefix=/home/usr1 --build=arm-verifone-linux-gnueabihf --host=x86_64-build_pc-linux-gnu --target=arm-verifone-linux-gnueabihf CC=arm-verifone-linux-gnueabihf-gcc \
    --disable-openssl \
    --disable-xxhash \
    --disable-zstd \
    --disable-lz4
    #--static
	make && make install
}
make_dirs
# download_package
tar_package
make_rsync

